package com.android.szkolenie.widoki;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class WidokiActivity extends ActionBarActivity {

    private static final String TAG = WidokiActivity.class.getName();

    private EditText mEditText;
//    private TextView mTextField;
    private Button mButton;
    private LinearLayout mLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            // Pierwsze otwarcie
        } else {
            // Obrot ekranu, po zmianie jezyka
            // po restarcie telefonu
        }

        // Ustawienie widoku z pliku xml layout/activity_widok.xml
        setContentView(R.layout.activity_widoki);

        // findViewById musi byc koniecznie po setContentView
        mEditText = findView(R.id.pole_tekstowe);
//        mTextField = (TextView) findViewById(R.id.label);
        mButton = (Button) findViewById(R.id.przycisk);
        mLinear = findView(R.id.teksty_kontener);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klikPrzycisk(view);
            }
        });
//        widokiZKodu();
    }

    public void widokiZKodu() {
        mLinear = new LinearLayout(this);
        mLinear.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        mLinear.setLayoutParams(mParams);

        // Wspolne artrybuty szerokosci i wysokosci dla pola tekstowego i edytowalnego
        LinearLayout.LayoutParams mKontrolkaParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mEditText = new EditText(this);
        mEditText.setLayoutParams(mKontrolkaParams);
        mEditText.setSingleLine(true);
        mEditText.setHint("Podaj tekst");

//        mTextField = new TextView(this);
//        mTextField.setLayoutParams(mKontrolkaParams);

        LinearLayout.LayoutParams mGuzikParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mButton = new Button(this); // findViewById
        mButton.setLayoutParams(mGuzikParams);
        mButton.setText("Gotowe");
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klikPrzycisk(view);
            }
        });

        // Dodanie kontrolek do linearLayout
        mLinear.addView(mEditText);
//        mLinear.addView(mTextField);
        mLinear.addView(mButton);

        // Wstawienie do kontenera obecnego w pliku XML
//        ((RelativeLayout)findViewById(R.id.kontener)).addView(mLinear);
        setContentView(mLinear); // Ustawienie całej treści na wygenerowany widok
    }

    public void klikPrzycisk(View mView) {
        LinearLayout.LayoutParams mKontrolkaParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        String text = mEditText.getText().toString();

        Log.d(TAG, text != null ? "Tekst ma dlugosc: "+text.length() : "Null !");

        if(text == null || text.isEmpty()) {
            Toast.makeText(this, "Nic nie wpisales !", Toast.LENGTH_LONG).show();
        } else if (text.length() < 3) {
            // Wyswietl ze za krotki tekst
            Toast.makeText(this, "Wpisałeś za krótki tekst", Toast.LENGTH_LONG).show();
            return;
        }
        mEditText.setText("");  // Czyscimy pole edycyjne
//        mTextField.setText(text);
        TextView mNowePole = new TextView(this);
        mNowePole.setLayoutParams(mKontrolkaParams);
        mNowePole.setText(text);

        mLinear.addView(mNowePole);
        mLinear.invalidate();
    }

    public <T extends View> T findView(int identyfikator) {
        return (T) findViewById(identyfikator);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }

    protected void wyczyscLinie() {
        List<View> mKolekcjaDoUsuniecia = new ArrayList<View>();
        for(int i = 0; i < mLinear.getChildCount(); i++) {
            View mChild = mLinear.getChildAt(i);
            mKolekcjaDoUsuniecia.add(mChild);
        }

        for(View mChild : mKolekcjaDoUsuniecia) {
            if(mChild.getClass().equals(TextView.class)) {
                if (mChild instanceof TextView && !(mChild instanceof EditText)
                        && !(mChild instanceof Button)) {
                    mLinear.removeView(mChild);
                }
            }
        }
    }

    protected void wyczyscOstatni() {
        // Bez pętli, bez kolekcji, 1-3 linijek
        View mChild = mLinear.getChildAt(mLinear.getChildCount() - 1);
        if(mChild instanceof TextView && !(mChild instanceof EditText)
                && !(mChild instanceof Button)) { mLinear.removeView(mChild); }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.usun_wszystkie) {
            wyczyscLinie();
            return true;
        }
        if(id == R.id.usun_ostatni) {
            wyczyscOstatni();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
